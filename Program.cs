﻿using System;
using System.Collections.Generic;
//3) Crea un programma contenente un metodo RImuoviPari() che elimini tutti i numeri pari da un vettore, e 
//    RimuoviDispari() che rimuove tutti i dispari.Per ogni metodo creare 2 versioni, 
//    una void (che modifica il vettore passato per riferimento) e una che invece restituisce una copia filtrata del vettore

namespace RipassoGeneraleDue
{

    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Inserisci quanti numeri vuoi nel vettore:"); int totalNumber = Convert.ToInt32(Console.ReadLine());
            int[] numbersArray = new int[totalNumber];
            int[] refreshNumbersArray = new int[totalNumber];
            int numbersArrayLong = numbersArray.Length;
            Random generatore = new Random();

            for (int i = 0; i < totalNumber; i++)
            {
                numbersArray[i] = generatore.Next(0, 1001);
                Console.WriteLine("Il numero {0}° del vettore è il {1}", i + 1, numbersArray[i]);
                refreshNumbersArray[i] = numbersArray[i];
            }

            VoidOddRemover(numbersArray,refreshNumbersArray); //passo perchè modifico il vettore nella funzione
            VoidEvenRemover(numbersArray,refreshNumbersArray);
            OddRemover(numbersArray);//non passo perchè lo returno filtrato al main

            for (int i = 0; i < numbersArrayLong; i++)
            {
                Console.WriteLine("Il valore del numero {0}° del vettore senza pari è {1}", i + 1, numbersArray[i]);
            }

            RefreshArray(numbersArray, refreshNumbersArray);
            EvenRemover(numbersArray);

            for (int i = 0; i < numbersArrayLong; i++)
            {
                Console.WriteLine("Il valore del numero {0}° del vettore senza dispari è {1}", i + 1, numbersArray[i]);
            }
        }

        static void VoidOddRemover(int[] numbersArray,int [] refreshNumbersArray)
        {
            int numbersArrayLong = numbersArray.Length;

            for (int i = 0; i < numbersArrayLong; i++)
            {
                if (numbersArray[i] % 2 == 0)
                {
                    numbersArray[i] = -1;
                }
            }
            Console.WriteLine("Sono la funzione VoidOddRemover");
            for (int i = 0; i < numbersArrayLong; i++)
            {
                Console.WriteLine("Il valore del numero {0}° del vettore senza pari è {1}", i + 1, numbersArray[i]);
            }
            RefreshArray(numbersArray, refreshNumbersArray);
        }

        static void VoidEvenRemover(int[] numbersArray, int[] refreshNumbersArray)
        {
            int numbersArrayLong = numbersArray.Length;

            for (int i = 0; i < numbersArrayLong; i++)
            {
                if (numbersArray[i] % 2 != 0)
                {
                    numbersArray[i] = -1;
                }
            }
            Console.WriteLine("Sono la funzione VoidEvenRemover");
            for (int i = 0; i < numbersArrayLong; i++)
            {
                Console.WriteLine("Il valore del numero {0}° del vettore senza dispari è {1}", i + 1, numbersArray[i]);
            }
            RefreshArray(numbersArray, refreshNumbersArray);
        }

        static int[] OddRemover(int[] numbersArray)
        {
            int numbersArrayLong = numbersArray.Length;

            for (int i = 0; i < numbersArrayLong; i++)
            {
                if (numbersArray[i] % 2 == 0)
                {
                    numbersArray[i] = -1;
                }
            }
            Console.WriteLine("Sono la funzione OddRemover");
            return numbersArray;
        }

        static int[] EvenRemover(int[] numbersArray)

        {
            int numbersArrayLong = numbersArray.Length;

            for (int i = 0; i < numbersArrayLong; i++)
            {
                if (numbersArray[i] % 2 != 0)
                {
                    numbersArray[i] = -1;
                }
            }
            Console.WriteLine("Sono la funzione EvenRemover");
            return numbersArray;
        }

        static int[] RefreshArray(int [] numbersArray,int [] refreshNumbersArray)
        {
            int numbersArrayLong = numbersArray.Length;

            for (int i = 0; i < numbersArrayLong; i++)
            {
                numbersArray[i] = refreshNumbersArray[i];
                Console.WriteLine("Il numero {0}° del vettore reimpostato è il {1}", i + 1, numbersArray[i]);
            }
            return numbersArray;
        }
    }



    //chiedo quanti numeri vuole nel vettore
            //creo il vettore con il totale 
                //inserisco i numeri positivi nel vettore
                    //stampo
    //creo il vettore per il reset
        //setto le due array uguali
    //creo la prima e la seconda funzione
        //inizio a richiamarle nel main
    //passo il vettore e il vettore per resettare dal main alla prima e alla seconda funzione
        //trasformo i numeri dispari/pari del vettore in -1
            //stampo il vettore modificato
                //resetto il vettore
    //creo la terza e la quarta
        //inizio a richiamarle nel main
    //passo il vettore dal main
        //trasformo i numeri dispari/pari del vettore in -1
            //passo il vettore al main
                //stampo il vettore modificato
                    //resetto il vettore (la penultima volta e basta)


}
